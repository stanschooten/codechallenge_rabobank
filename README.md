# codechallenge_rabobank

Time stopped at: 4 hours and 7 minutes

## Getting started

To start this application, install the maven dependencies and create a local postgresql database. The credentials for this database are: 

`Database name: codechallenge`

`Username: codechallenge`

`password: codechallenge`

In the application.yaml file, these properties are customizable. 

The application will create the tables in the database when it is started the first time. When the environment variable "dev" is added multiple test users will be added, the credentials of these users are: 

```
username: user1
password: admin
```

```
username: user2
password: admin
```

```
username: admin
password: admin
```

user1 and user 2 have the user roles and the admin user has the ADMIN roles. 

## The API 
The application has multiple endpoints, because of the time constraints I didn't implement OPEN API, but I did include my postman file in the root folder that you can use. When you login with one of the credentials from above, don't forget to use the token as a bearer token in postman. 

The endpoints that are available within the application: 
- Create a user (register)
- Login 
- Get logged in user 
- Get all users (admin only)
- Update logged in user
- Delete a user (admin only)
- Create a transaction 
