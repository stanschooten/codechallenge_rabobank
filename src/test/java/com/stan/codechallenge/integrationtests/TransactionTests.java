package com.stan.codechallenge.integrationtests;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.stan.codechallenge.IntegrationTestClass;
import com.stan.codechallenge.domain.dto.transaction.TransactionRequest;
import com.stan.codechallenge.domain.dto.user.CreateUserRequest;
import com.stan.codechallenge.domain.dto.user.UpdateUserRequest;
import com.stan.codechallenge.domain.dto.user.UserDto;
import com.stan.codechallenge.domain.model.User;
import com.stan.codechallenge.repositories.UserRepository;
import com.stan.codechallenge.services.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.List;

import static org.hibernate.validator.internal.util.Contracts.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser(username = "testuser1", roles = {"ADMIN"})
public class TransactionTests extends IntegrationTestClass {
    @Autowired
    private UserService userService;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private MockMvc mockMvc;

    private User user1;
    private User user2;

    @BeforeEach
    public void setUp() {
        String password = "testPassword";
        CreateUserRequest firstUser = new CreateUserRequest("testuser1", "testuser1@user1.com", 10, password);
        user1 = userService.createUser(firstUser);

        CreateUserRequest secondUser = new CreateUserRequest("testuser2", "testuser2@user2.com", 10, password);
        user2 = userService.createUser(secondUser);
    }

    @Test
    public void testTransaction() {
        ObjectMapper objectMapper = new ObjectMapper();
        TransactionRequest updateUserRequest = new TransactionRequest(user2.getUsername(), 5);

        try {
            this.mockMvc
                    .perform(post("/api/transaction")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(updateUserRequest)))
                    .andExpect(status().isOk());
        } catch (Exception e) {
            assertNotNull(e.toString());
        }

        assertEquals(userService.findUserByName(user1.getUsername()).getAmount(), 5, "Amount isn't equal!");
        assertEquals(userService.findUserByName(user2.getUsername()).getAmount(), 15, "Amount isn't equal!");
    }

    @Test
    public void testTransactionWithInsufficientAmount() {
        ObjectMapper objectMapper = new ObjectMapper();
        TransactionRequest updateUserRequest = new TransactionRequest(user2.getUsername(), 15);

        try {
            this.mockMvc
                    .perform(post("/api/transaction")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(updateUserRequest)))
                    .andExpect(status().isOk());
        } catch (Exception e) {
            assertNotNull(e.toString());
        }
    }

    @Test
    public void testTransactionWithNonExistingTarget() {
        ObjectMapper objectMapper = new ObjectMapper();
        TransactionRequest updateUserRequest = new TransactionRequest("nonExistingUser", 15);

        try {
            this.mockMvc
                    .perform(post("/api/transaction")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(updateUserRequest)))
                    .andExpect(status().isOk());
        } catch (Exception e) {
            assertNotNull(e.toString());
        }
    }

    @Test
    public void testTransactionWithIncorrectFormat() {
        ObjectMapper objectMapper = new ObjectMapper();
        TransactionRequest updateUserRequest = new TransactionRequest(user2.getUsername(), 5.00001);

        try {
            this.mockMvc
                    .perform(post("/api/transaction")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(updateUserRequest)))
                    .andExpect(status().isOk());
        } catch (Exception e) {
            assertNotNull(e.toString());
        }
    }
}
