package com.stan.codechallenge.integrationtests;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.stan.codechallenge.IntegrationTestClass;
import com.stan.codechallenge.domain.dto.user.CreateUserRequest;
import com.stan.codechallenge.domain.dto.user.DeleteUserRequest;
import com.stan.codechallenge.domain.dto.user.UpdateUserRequest;
import com.stan.codechallenge.domain.dto.user.UserDto;
import com.stan.codechallenge.domain.model.User;
import com.stan.codechallenge.services.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Collections;
import java.util.List;

import static org.hibernate.validator.internal.util.Contracts.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser(username = "testuser1", roles = {"ADMIN"})
public class UserTests extends IntegrationTestClass {

    @Autowired
    private UserService userService;
    @Autowired
    private MockMvc mockMvc;

    private User user1;
    private User user2;

    @BeforeEach
    public void setUp() {
        String password = "testPassword";
        CreateUserRequest firstUser = new CreateUserRequest("testuser1", "testuser1@user1.com", 10, password, Collections.singleton("ADMIN"));
        user1 = userService.createUser(firstUser);

        CreateUserRequest secondUser = new CreateUserRequest("testuser2", "testuser2@user2.com", 10, password);
        user2 = userService.createUser(secondUser);
    }

    @Test
    public void testGetCurrentUser() throws Exception {
        MvcResult getResult = this.mockMvc
                .perform(get("/api/user"))
                .andExpect(status().isOk())
                .andReturn();

        ObjectMapper objectMapper = new ObjectMapper();
        UserDto response = objectMapper.readValue(getResult.getResponse().getContentAsString(), UserDto.class);

        assertEquals(getResult.getResponse().getStatus(), 200, "Request not successful");
        assertEquals(response.email(), user1.getEmail(), "Request not successful");
    }

    @Test
    public void testGetAllUser() throws Exception {
        MvcResult getResult = this.mockMvc
                .perform(get("/api/user/all"))
                .andExpect(status().isOk())
                .andReturn();

        ObjectMapper objectMapper = new ObjectMapper();
        List<UserDto> response = objectMapper.readValue(getResult.getResponse().getContentAsString(), new TypeReference<List<UserDto>>() {
        });

        assertEquals(getResult.getResponse().getStatus(), 200, "Request not successful");
        assertEquals(response.size(), 2, "Request not successful");
    }

    @Test
    public void testDeleteUser() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        DeleteUserRequest deleteUserRequest = new DeleteUserRequest(user1.getId());

        this.mockMvc
                .perform(delete("/api/user")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(deleteUserRequest)))
                .andExpect(status().isOk());
    }

    @Test
    public void testDeleteNonExistingUser() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        DeleteUserRequest deleteUserRequest = new DeleteUserRequest(Integer.MAX_VALUE);

        try {
            this.mockMvc
                    .perform(delete("/api/user")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(deleteUserRequest)));
        } catch (Exception e) {
            assertNotNull(e.toString());
        }
    }

    @Test
    public void testUpdatingUser() {
        ObjectMapper objectMapper = new ObjectMapper();
        UpdateUserRequest updateUserRequest = new UpdateUserRequest("newUsername", "newEmail", "newPassword");

        try {
            this.mockMvc
                    .perform(put("/api/user")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(updateUserRequest)))
                    .andExpect(status().isOk());
        } catch (Exception e) {
            assertNotNull(e.toString());
        }

        assertEquals(userService.findUserByName(user1.getUsername()).getUsername(), "newUsername", "User hasn't updated!");
    }

    @Test
    public void testUpdatingUserWithUsedCredentials() {
        ObjectMapper objectMapper = new ObjectMapper();
        UpdateUserRequest updateUserRequest = new UpdateUserRequest(user1.getUsername(), null, null);

        try {
            this.mockMvc
                    .perform(put("/api/user")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(updateUserRequest)));
        } catch (Exception e) {
            assertNotNull(e.toString());
        }
    }
}
