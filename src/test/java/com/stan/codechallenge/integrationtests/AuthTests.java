package com.stan.codechallenge.integrationtests;

import com.stan.codechallenge.IntegrationTestClass;
import com.stan.codechallenge.controllers.AuthController;
import com.stan.codechallenge.domain.dto.user.AuthRequest;
import com.stan.codechallenge.domain.dto.user.CreateUserRequest;
import com.stan.codechallenge.domain.dto.user.UserDto;
import com.stan.codechallenge.domain.model.User;
import com.stan.codechallenge.services.UserService;
import jakarta.validation.ValidationException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.hibernate.validator.internal.util.Contracts.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class AuthTests extends IntegrationTestClass {

    @Autowired
    private UserService userService;

    @Autowired
    private AuthController authController;

    private User user1;
    private User user2;

    private final String password = "testPassword";

    @BeforeEach
    public void setUp() {

        CreateUserRequest firstUser = new CreateUserRequest("testuser1", "testuser1@user1.com", 10, password);
        user1 = userService.createUser(firstUser);

        CreateUserRequest secondUser = new CreateUserRequest("testuser2", "testuser2@user2.com", 10, password);
        user2 = userService.createUser(secondUser);
    }

    @Test
    public void testLoginSuccess() {
        AuthRequest request = new AuthRequest(user1.getUsername(), password);

        ResponseEntity<String> response = authController.login(request);

        assertEquals(response.getStatusCode(), HttpStatus.OK, "Request not successful");
    }

    @Test
    public void testLoginFail() {
        AuthRequest request = new AuthRequest("invalidname", "invalidpassword");

        ResponseEntity<String> response = authController.login(request);
        assertEquals(response.getStatusCode(), HttpStatus.UNAUTHORIZED, "Request not successful");
    }

    @Test
    public void testRegisterSuccess() {
        String userName = "testingUser";
        CreateUserRequest testingUser = new CreateUserRequest(userName, "testingUser", 10, password);
        UserDto userDto = authController.register(testingUser);

        assertNotNull(userDto.id(), "User id must not be null!");
        assertEquals(testingUser.username(), userDto.username(), "Username isn't equal!");
    }

    @Test
    public void testRegisterFail() {
        try {
            CreateUserRequest testingUser = new CreateUserRequest(user1.getUsername(), user1.getEmail(), 10, password);
            authController.register(testingUser);
        } catch (ValidationException e) {
            assertNotNull(e.toString());
        }
    }
}