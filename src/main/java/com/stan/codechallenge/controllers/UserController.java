package com.stan.codechallenge.controllers;

import com.stan.codechallenge.domain.dto.user.DeleteUserRequest;
import com.stan.codechallenge.domain.dto.user.UpdateUserRequest;
import com.stan.codechallenge.domain.dto.user.UserDto;
import com.stan.codechallenge.domain.model.Role;
import com.stan.codechallenge.services.UserService;
import jakarta.annotation.security.RolesAllowed;
import jakarta.validation.Valid;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/user")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    @RolesAllowed({Role.USER, Role.ADMIN})
    public UserDto getCurrentUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        return userService
                .findUserByName(authentication.getName())
                .toDto();
    }

    @PutMapping
    @RolesAllowed({Role.USER, Role.ADMIN})
    public UserDto updateUser(@RequestBody @Valid UpdateUserRequest request) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return userService.updateUser(authentication.getName(), request);
    }

    @DeleteMapping
    @RolesAllowed(Role.ADMIN)
    public void getAllUsers(@RequestBody @Valid DeleteUserRequest request) {
        userService.deleteUser(request);
    }

    @GetMapping("all")
    @RolesAllowed(Role.ADMIN)
    public List<UserDto> getAllUsers() {
        return userService.getAllUsers();
    }
}
