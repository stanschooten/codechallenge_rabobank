package com.stan.codechallenge.controllers;

import com.stan.codechallenge.domain.dto.transaction.TransactionRequest;
import com.stan.codechallenge.domain.model.Role;
import com.stan.codechallenge.services.TransactionService;
import jakarta.annotation.security.RolesAllowed;
import jakarta.validation.Valid;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/transaction")
@RolesAllowed({Role.USER, Role.ADMIN})
public class TransactionController {

    private final TransactionService transactionService;

    public TransactionController(TransactionService moneyService) {
        this.transactionService = moneyService;
    }

    @PostMapping
    public double transferMoney(@RequestBody @Valid TransactionRequest request) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return transactionService.transaction(authentication.getName(), request);
    }
}
