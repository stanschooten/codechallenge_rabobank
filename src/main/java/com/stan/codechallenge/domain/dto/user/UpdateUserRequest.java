package com.stan.codechallenge.domain.dto.user;


public record UpdateUserRequest(
        String username,
        String email,
        String password
) {
}
