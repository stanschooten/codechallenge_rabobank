package com.stan.codechallenge.domain.dto.transaction;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

public record TransactionRequest(
        @NotBlank String targetUser,
        @NotNull double amount
) {
}
