package com.stan.codechallenge.domain.dto.user;

import jakarta.validation.constraints.NotNull;

public record DeleteUserRequest(
        @NotNull long id
) {
}
