package com.stan.codechallenge.domain.dto.user;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

import java.util.HashSet;
import java.util.Set;

public record CreateUserRequest(
        @NotBlank String username,
        @NotBlank String email,
        @NotNull double openingAmount,
        @NotBlank String password,
        Set<String> authorities
) {
    public CreateUserRequest {
        if (authorities == null) {
            authorities = new HashSet<>();
        }
    }



    public CreateUserRequest(String username, String email, double openingAmount, String password) {
        this(username, email, openingAmount, password, new HashSet<>());
    }
}