package com.stan.codechallenge.domain.dto.user;

public record UserDto(
        long id,
        String username,
        String email,
        double amount
) {
}