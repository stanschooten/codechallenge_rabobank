package com.stan.codechallenge.domain.exception;

public class IncorrectFormatException extends RuntimeException{

    public IncorrectFormatException(String message) {
        super(message);
    }

    public IncorrectFormatException(double amount) {
        super(String.format("Your current amount is %s but should be %,.2f", amount, amount));
    }
}
