package com.stan.codechallenge.domain.exception;

public class InsufficientMoneyException extends RuntimeException {

    public InsufficientMoneyException(String message) {
        super(message);
    }

    public InsufficientMoneyException(double currentAmount, double desiredAmount) {
        super(String.format("Your current amount of %,.2f is not sufficient to transfer an amount of %,.2f", currentAmount, desiredAmount));
    }
}
