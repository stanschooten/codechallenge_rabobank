package com.stan.codechallenge.domain.exception;

public class NotFoundException extends RuntimeException {

    public NotFoundException(String message) {
        super(message);
    }

    public NotFoundException(Class<?> entity, long id) {
        super(String.format("Entity %s with id %d not found", entity.getSimpleName(), id));
    }

    public NotFoundException(Class<?> entity, String id) {
        super(String.format("Entity %s with id %s not found", entity.getSimpleName(), id));
    }
}