package com.stan.codechallenge.domain.model;

import com.stan.codechallenge.domain.dto.user.UserDto;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import static java.util.stream.Collectors.toSet;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "\"user\"")
public class User implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(unique = true)
    private String username;

    @Column(unique = true)
    private String email;

    private double amount;

    private String password;

    private LocalDateTime createdAt;

    private LocalDateTime modifiedAt;

    private boolean enabled = true;

    @ElementCollection
    private Set<Role> authorities = new HashSet<>();

    public User(String username, String email, double amount, Set<String> authorities) {
        this.username = username;
        this.email = email;
        this.amount = amount;
        this.authorities = authorities.stream().map(Role::new).collect(toSet());
        this.modifiedAt = LocalDateTime.now();
        this.createdAt = LocalDateTime.now();
    }

    public User(String username, String email, double amount) {
        this.username = username;
        this.email = email;
        this.amount = amount;
        this.modifiedAt = LocalDateTime.now();
        this.createdAt = LocalDateTime.now();
    }

    public UserDto toDto() {
        return new UserDto(this.id, this.username, this.email, this.amount);
    }

    public double transferAmount(double amount) {
        this.amount += amount;
        return this.amount;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.authorities;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return this.enabled;
    }
}