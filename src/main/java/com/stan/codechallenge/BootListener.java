package com.stan.codechallenge;

import com.stan.codechallenge.domain.dto.user.CreateUserRequest;
import com.stan.codechallenge.repositories.UserRepository;
import com.stan.codechallenge.services.UserService;
import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.Collections;

@Component
@Slf4j
@Profile("dev")
public class BootListener {
    private final UserRepository userRepository;

    private final UserService userService;

    public BootListener(UserRepository userRepository, UserService userService) {
        this.userRepository = userRepository;
        this.userService = userService;
    }

    @EventListener
    @Transactional
    public void bootListenerDevelop(ApplicationStartedEvent event) {
        log.warn("Instance is running as a DEVELOP instance!");

        if (userRepository.count() == 0) {
            log.info("Creating seed data...");

            CreateUserRequest firstUser = new CreateUserRequest("user1", "user1@user1.com", 10, "admin");
            userService.createUser(firstUser);

            CreateUserRequest secondUser = new CreateUserRequest("user2", "user2@user2.com", 10, "admin");
            userService.createUser(secondUser);

            CreateUserRequest admin = new CreateUserRequest("admin", "admin@admin.com", 10, "admin", Collections.singleton("ADMIN"));
            userService.createUser(admin);

            log.info("Created seed data!");
        }
    }
}
