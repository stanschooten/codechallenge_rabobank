package com.stan.codechallenge.services;

import com.stan.codechallenge.domain.dto.transaction.TransactionRequest;
import com.stan.codechallenge.domain.exception.IncorrectFormatException;
import com.stan.codechallenge.domain.exception.InsufficientMoneyException;
import com.stan.codechallenge.domain.model.User;
import com.stan.codechallenge.repositories.UserRepository;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;

@Service
public class TransactionService {

    private final UserRepository userRepository;
    private final UserService userService;

    public TransactionService(UserRepository userRepository, UserService userService) {
        this.userRepository = userRepository;
        this.userService = userService;
    }

    @Transactional
    public double transaction(String currentUsername, TransactionRequest request) {
        this.checkAmountFormat(request.amount());

        User user = userService.findUserByName(currentUsername);

        if (request.amount() > user.getAmount()) {
            throw new InsufficientMoneyException(user.getAmount(), request.amount());
        }

        User targetUser = userService.findUserByName(request.targetUser());

        targetUser.transferAmount(request.amount());
        double remainingAmount = user.transferAmount(request.amount() * -1);

        userRepository.save(user);
        userRepository.save(targetUser);

        return remainingAmount;
    }

    private void checkAmountFormat(double amount) {
        String text = Double.toString(Math.abs(amount));
        int decimalPlaces = text.length() - text.indexOf('.') - 1;

        if (decimalPlaces > 2) {
            throw new IncorrectFormatException(amount);
        }
    }
}
