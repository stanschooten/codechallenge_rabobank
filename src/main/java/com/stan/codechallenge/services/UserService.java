package com.stan.codechallenge.services;

import com.stan.codechallenge.domain.dto.user.CreateUserRequest;
import com.stan.codechallenge.domain.dto.user.DeleteUserRequest;
import com.stan.codechallenge.domain.dto.user.UpdateUserRequest;
import com.stan.codechallenge.domain.dto.user.UserDto;
import com.stan.codechallenge.domain.exception.NotFoundException;
import com.stan.codechallenge.domain.model.User;
import com.stan.codechallenge.repositories.UserRepository;
import jakarta.transaction.Transactional;
import jakarta.validation.ValidationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public User findUserByName(String username) {
        return userRepository
                .findByUsername(username)
                .orElseThrow(() -> new NotFoundException(User.class, username));
    }

    @Transactional
    public User createUser(CreateUserRequest request) {
        if (userRepository.findByUsername(request.username()).isPresent()) {
            throw new ValidationException("Username already exists!");
        }

        if (userRepository.findByEmail(request.email()).isPresent()) {
            throw new ValidationException("Email already exists!");
        }

        if (request.authorities().isEmpty()) {
            request.authorities().add("USER");
        }

        User user = new User(request.username(), request.email(), request.openingAmount(), request.authorities());
        user.setPassword(passwordEncoder.encode(request.password()));

        user = userRepository.save(user);

        return user;
    }

    @Transactional
    public UserDto updateUser(String username, UpdateUserRequest request) {
        User user = findUserByName(username);

        if (!request.email().equals(user.getUsername()) && userRepository.findByUsername(request.username()).isPresent()) {
            throw new ValidationException("Username already exists!");
        }

        if (!request.email().equals(user.getEmail()) && userRepository.findByEmail(request.email()).isPresent()) {
            throw new ValidationException("Email already exists!");
        }

        if (!request.password().isBlank()) {
            user.setPassword(passwordEncoder.encode(request.password()));
        }

        if (!request.email().isBlank()) {
            user.setEmail(request.email());
        }

        if (!request.username().isBlank()) {
            user.setUsername(request.username());
        }

        user = userRepository.save(user);

        return user.toDto();
    }

    @Transactional
    public List<UserDto> getAllUsers() {
        List<User> users = userRepository.findAll();

        return users
                .stream()
                .map(User::toDto)
                .collect(Collectors.toList());
    }

    @Transactional
    public void deleteUser(DeleteUserRequest request) {
        userRepository.deleteById(request.id());
    }
}